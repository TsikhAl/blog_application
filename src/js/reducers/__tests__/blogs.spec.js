import test from 'tape';
import { loop, Cmd } from 'redux-loop';
import { initBlogs, blogsInitSuccessful, blogsInitFail, requestdeleteBlog, 
	deleteBlog, requestAddBlog, addBlog, requestDeleteBlog, failedRequest } from '../../actions';
import blogs from '../blogs.js';
import { actionTypes } from '../../config.js';

describe('blogs reducer', () => {
	const initialState = [],
		dummyBlog = {
			author: 'author',
			title: 'title',
			description: 'description',
			date: '2018-03-01T13:37:08.068Z'
		},
		dummyBlogsArray = [{}, {}],
		dummyState = [
			{
				id: 123,
				blog: {
					author: 'author',
					title: 'title',
					description: 'description',
					date: '2018-03-01T13:37:08.068Z'
				}
			},
			{
				id: 1234,
				blog: {
					author: 'author2',
					title: 'title2',
					description: 'description2',
					date: '2018-03-01T13:38:08.068Z'
				}
			},
		];

	it('should handle initial state', () => {
		expect(blogs(undefined, {})).toEqual(initialState);
	});

	it('should handle ADD_BLOG', () => {
		expect(blogs(initialState, {
			type: actionTypes.ADD_BLOG,
			blog: dummyBlog,
			id: 123
		})).toEqual([{id: 123, blog: dummyBlog}])
	});

	it('should handle DELETE_BLOG', () => {
		expect(blogs([{id: 123, blog: dummyBlog}], {
			type: actionTypes.DELETE_BLOG,
			id: 123
		})).toEqual([])
	});
});
import React from "react";
import Portal from '../../../Portal.jsx'

class AddBlogModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            author: '',
            description: '',
            date: ''
        }
    }

    handleInputChange(e) {
        const name = e.target.name;
        const value = e.target.value;

        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.title || !this.state.author || !this.state.description || !this.state.date) return;
        this.props.onConfirm({
            title: this.state.title,
            author: this.state.author,
            description: this.state.description,
            date: this.state.date
        })
        this.props.history.push('/blogs');
    }
        
    onClose(e) {
        		e.preventDefault();
        		this.props.history.push('/blogs');
    }

    render() {
        return (
            <Portal>
                <div className="addBlogModal">
                    <a href="#" className="closeBtn" onClick={(e) => this.onClose(e)}> X </a>
                    <form className="addBlogForm" onSubmit={(e) => this.handleSubmit(e)}>
                        <label htmlFor="inputTitle">Title:</label>
                        <input name="title" id="inputTitle" type="text" value={this.state.title} onChange={(e) => this.handleInputChange(e)} />
                        <label htmlFor="inputAuthor">Author:</label>
                        <input name="author" id="inputAuthor" type="text" value={this.state.author} onChange={(e) => this.handleInputChange(e)} />
                        <label htmlFor="inputDescription">Description:</label>
                        <textarea name="description" id="inputDescription" value={this.state.description} onChange={(e) => this.handleInputChange(e)} />
                        <label htmlFor="inputDate">Date:</label>
                        <input name="date" id="inputDate" type="text" value={this.state.date} onChange={(e) => this.handleInputChange(e)} />
                        <button className="addBlogBtn btn">Add blog</button>
                    </form>
                </div>
            </Portal>
        );
    }
}

export default AddBlogModal;
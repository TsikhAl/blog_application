import React from 'react';
import { Link } from 'react-router-dom';

const AddBlogButton = ({onBlogAdd}) => (
	<Link to="/blogs/add" className="addBlogBtn btn">Add blog</Link>
)

export default AddBlogButton;
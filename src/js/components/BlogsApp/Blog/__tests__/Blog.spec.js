import React from 'react';
import Blog from '../index.jsx';

describe('Blog component', () => {
	const dummyBlog = {
		title: 'bla',
		author: 'blabla',
        description: '50cent',
        date: '12-12-2018'
	};
	const dummyBlogId = 123;

	it('renders correctly', () => {
		const wrapper = shallow(<Blog blog={dummyBlog} id={dummyBlogId}/>);
		expect(wrapper).toMatchSnapshot();
	});
})
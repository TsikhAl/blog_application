import React from "react";
import { Link } from 'react-router-dom';

const Blog = ({blog, blogId}) => (
	<li className="blogCont">
		<dl className="blogInfo">
			<dt> Title: </dt>
			<dd> {blog.title} </dd>
			<dt> Author: </dt>
			<dd> {blog.author} </dd>
			<dt> Description: </dt>
			<dd> {blog.description}</dd>
            <dt> Date: </dt>
			<dd> {blog.date}</dd>
		</dl>
		<Link to={`/blogs/remove/${blogId}`} className="removeBlog"> X </Link>
		<Link className="blogDetailsLink" to={`/blog/${blogId}`}> Blog details </Link>
	</li>
)

export default Blog; 
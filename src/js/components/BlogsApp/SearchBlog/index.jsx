import React from "react";

const SearchBlogBy = ({search, onValueChange}) => (
			<div className="searchBlog">
				<input className="searchBlogInput" type="text" placeholder="Search" value={search.searchValue} onChange={(e) => onValueChange(e.target.value)} />
			</div>
);

export default SearchBlogBy;